import time
import nmap
import configparser

def scan_network(network):
    nm = nmap.PortScanner()
    nm.scan(hosts=network, arguments='-p 22')  # Adjust the port number as needed

    ip_list = []
    for host in nm.all_hosts():
        if nm[host].has_tcp(22) and nm[host]['tcp'][22]['state'] == 'open':
            ip_list.append(host)

    return ip_list

def create_ini_file(ip_list, ini_filename):
    config = configparser.ConfigParser()

    for index, ip in enumerate(ip_list, start=1):
        section_name = f"Host_{index}"
        config.add_section(section_name)
        config.set(section_name, 'ip', ip)

    with open(ini_filename, 'w') as configfile:
        config.write(configfile)

def main():
    network_to_scan =  "192.168.1.1-255"  # Set your adresse, example :192.168.1.1-255
    inventory_ini_filename = "inventory.ini"  # Replace with your desired filename

    ip_list = scan_network(network_to_scan)
    create_ini_file(ip_list, inventory_ini_filename)

    print(f"Scanned IPs: {ip_list}")
    print(f"Inventory file '{inventory_ini_filename}' created successfully.")


while True:
    main()
    time.sleep(120)