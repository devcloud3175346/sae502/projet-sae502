# Rapports des tests réalisés sur l'application

Cette page détaille l'ensemble des tests que nous avons réalisés pour nous assurer que l'application est fonctionnelle et que les parties ajoutées remplissent leur rôle.

Les tests seront répartis en fonction des fonctionnalités ajoutées.

## Tests sur la récolte de données de VM

Le coeur de l'application et donc la partie que nous avons développée en première est la récolte d'information (avec le fichier python "collector.py"). C'est donc également la partie qui a été testée en première.

Pour tester la première version de notre collecteur de données nous avons utilisé des machines virtuelles que nous avons lancées en local sur un ordinateur de test. Une fois la machine virtuelle créée nous avons testé de récupérer plusieurs informations, à commencer par la charge processeur.

Les valeurs obtenues étaient dans un premier temps affichées dans la console python, nous pouvions donc assez facilement vérifier si les informations récoltées étaient valides.

Pour valider ce test nous avons également provoqué de l'activité sur la machine virtuelle (en lançant une application ou en effectuant des requêtes avec la commande ping) pour vérifier si les données s'actualisent bien en temps réel.

**Tests Validés**

## Tests sur l'interface graphique

Le premier test à réaliser est de tester si l'interface graphique est accessible via un navigateur. Nous utilisons le module Flask pour créer des pages web sur notre machine locale. Après avoir exposé un port nous avons pu constater que l'interface graphique est bien joignable.


Le second tests est de vérifier que l'interface graphique accède bien aux données récoltées par notre collecteur. Nous avons pour dela réalisé un tableau contenant, sous forme très basique, les informations sur les machines supervisées.

**Tests Validés**

Une fois ces tests réalisés nous avons travaillé sur la partie graphique de l'interface. Aucun tests n'a été réalisé, à part ceux ocnsistant à vérifier si la page était toujours accessible et observer les changements entre les différentes versions de l'interface.

Les derniers tests réalisés sont ceux de l'intégration des graphiques. Nous avons particulièrement observé la cohérence et les taux de rafraichissement des données.

**Tests Validés**

## Tests de notre système d'alarme

Pour tester le fonctionnement de notre alerteur nous avons définit des valeurs de seuils faibles pour que les remontées d'alarmes se fassent plus facilment. Nous avons ainsi pu constater que les données de charge des équipements sont bien récupérées et qu'une action est réalisée quand ces valeurs dépassent les seuils configurés.

**Tests Validés**

La seconde partie de l'alerteur consiste à envoyer un message ou un mail à un destinataire paramétré pour lui signaler l'incident. Nous avons pour cela utilisé un module python avec des identifiants de tests créés pour les tests. Nous n'avons cependant pas réussi à faire aboutir cette partie pour le moment, nous reviendrons dessus dans un prochain sprint.

**Défaut constaté :** Problème d'authentification auprès du serveur d'envoi

**Tests Non Validés**

## Tests sur le module de découverte

Nous avons tenté d'implémenter un module permettant de découvrir automatiquement les hôtes présents sur le réseau pour les ajouter à l'inventaire des machines supervisées. Ce module est encore en cours de développement mais nous avons déjà réalisé quelques tests pour valider notre avancée.

La majeure partie des tests a consisté à vérifier si les hôtes sont bien découverts. Nous avons pu valider ces tests en plaçant des machines virtuelles sur le réseau et en lançant notre programme.

**Tests Validés :** Résultats du scan ajoutés à un fichier

La suite de ce module est de fournir le fichier créé par le scan au programme de collecte des données. Ainsi ce dernier pourra directement lire le fichier et récupérer les informations des machines précédemment scanées.

**Partie non terminée, tests non réalisés**