# Guide d'utilisation du Tableau de Bord - Machines Virtuelles
Ce guide vous aidera à comprendre comment utiliser le tableau de bord des machines virtuelles.

## Page d'accueil
Lorsque vous ouvrez le tableau de bord, vous êtes accueilli par un en-tête noir avec le titre "Bienvenue sur votre tableau de bord".

## Tableau des machines virtuelles
Juste en dessous de l'en-tête, vous trouverez un tableau qui affiche l'état des machines virtuelles en temps réel. Ce tableau est mis à jour toutes les 30 secondes.

Le tableau contient les informations suivantes pour chaque machine virtuelle :

- ID : L'identifiant unique de la machine virtuelle.
- CPU Use (%) : Le pourcentage d'utilisation du CPU.
- RAM Use (%) : Le pourcentage d'utilisation de la RAM.
- Total RAM (MB) : La quantité totale de RAM en mégaoctets.
- Free RAM (MB) : La quantité de RAM libre en mégaoctets.
- Network : Les statistiques du réseau. Cliquez sur "Statistiques Réseau" pour les voir.
- Disk Use (%) : Le pourcentage d'utilisation du disque.
- Total Disk Space (GB) : L'espace disque total en gigaoctets.
- Free Disk Space (GB) : L'espace disque libre en gigaoctets.

## Graphique en temps réel
En dessous du tableau, vous trouverez un graphique en temps réel qui affiche l'utilisation du CPU, de la RAM ou du disque pour chaque machine virtuelle. Vous pouvez sélectionner la métrique que vous souhaitez afficher à l'aide du menu déroulant situé au-dessous du graphique. Le graphique est mis à jour toutes les minutes.

Le graphique affiche deux lignes, une pour chaque machine virtuelle (VM1 et VM2). Les dates et heures sont affichées sur l'axe des x, et le pourcentage d'utilisation est affiché sur l'axe des y.

> Des mises à jour viendront prochainement compléter les fonctionnalités apportées par ce Tableau de Bord.

## Bugs
En cas de problème ou de bug constaté sur le tableau de bord, vous pouvez ouvrir un ticket sur GitHub ou adresser un mail à **saedevcloud@gmail.com**.