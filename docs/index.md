## Présentation du Projet: Application de Supervision de VM

### Introduction
L'application de supervision de VM est un outil conçu pour permettre aux administrateurs système, aux opérateurs réseau, aux développeurs et à d'autres parties prenantes de surveiller et de gérer efficacement les performances, les logs et les ressources des machines virtuelles (VM) dans un environnement informatique. Cette application offre une interface conviviale et intuitive qui permet aux utilisateurs d'accéder rapidement à des informations essentielles sur l'état de leurs VMs, de recevoir des alertes en temps réel en cas de problème critique, et de générer des rapports détaillés pour évaluer les performances de l'infrastructure.

### Objectifs du Projet
1. Développer une application de supervision de VM robuste et fiable.
2. Fournir des fonctionnalités complètes pour surveiller les métriques de performance des VM, les logs d'application et les ressources système.
3. Permettre aux utilisateurs de personnaliser leur expérience en choisissant les données à afficher sur le tableau de bord et en configurant des alertes personnalisées.
4. Assurer la sécurité et la conformité en mettant en place des mécanismes d'authentification et d'autorisation robustes, ainsi que des fonctionnalités de suivi des accès aux données sensibles.

### Fonctionnalités Principales
**Collector (Collecte de Données):**
   - Collecte des données de performance (CPU, mémoire, espace disque, carte réseau) à partir des VMs et des hôtes physiques.
   - Intégration de mécanismes pour récupérer les logs d'application des VMs et les stocker pour une analyse ultérieure.
   - Mise en place d'un système de découverte automatique des VMs et des PC sur le réseau.

**Alerter (Système d'Alerte):**
   - Configuration de seuils d'alerte pour détecter les performances anormales ou les erreurs critiques.
   - Envoi de notifications en temps réel aux administrateurs ou aux responsables concernés en cas de dépassement de seuils prédéfinis.
   
**Front End (Interface Utilisateur):**
   - Développement d'une interface utilisateur conviviale et intuitive pour visualiser les métriques, les logs et les rapports.
   - Personnalisation de l'interface utilisateur pour permettre aux utilisateurs de sélectionner les informations à afficher et de configurer leur tableau de bord selon leurs besoins.
   
**Discover (Découverte de VM ou de PC sur le Réseau):** (Fonction en développement)
   - Implémentation de fonctionnalités pour découvrir automatiquement les VMs ou les PC sur le réseau.
   - Intégration de mécanismes pour ajouter facilement de nouveaux hôtes au système de supervision.

### Plan de Développement
Le projet sera réalisé en sprints itératifs, avec chaque sprint se concentrant sur des fonctionnalités spécifiques. Chaque sprint comprendra des phases de planification, de développement, de test et de déploiement pour assurer la qualité et la cohérence du produit final. La communication et la collaboration entre les membres de l'équipe seront essentielles pour suivre efficacement la progression du projet et pour résoudre les problèmes qui pourraient survenir en cours de route.

L'ensemble des sprints sont plus détaillés sur notre outils de gestion de projet, [Trello](https://trello.com/b/5SkTCmDS/backlog). 
De plus vous retrouverez l'ensemble du Backlog sur celui-ci ainsi qu'un diagramme de Gantt prévisionnel sur le projet.