# Documentation Technique pour le script `discover.py`

Ce script Python utilise la bibliothèque `nmap` pour scanner un réseau et découvrir les hôtes qui ont le port 22 ouvert. Il génère ensuite un fichier INI avec les adresses IP de ces hôtes.

## Dépendances

- `nmap` : Une bibliothèque Python pour utiliser le scanner de port Nmap.
- `configparser` : Une bibliothèque Python pour travailler avec les fichiers INI.
- `time` : Une bibliothèque Python pour contrôler le délai entre les scans de réseau.

## Fonctions

### `scan_network(network)`

Cette fonction scanne le réseau spécifié pour les hôtes qui ont le port 22 ouvert. Elle retourne une liste des adresses IP de ces hôtes.

### `create_ini_file(ip_list, ini_filename)`

Cette fonction crée un fichier INI à partir d'une liste d'adresses IP. Chaque adresse IP est ajoutée à une section distincte du fichier INI.

### `main()`

Cette fonction est le point d'entrée du script. Elle définit le réseau à scanner et le nom du fichier INI à générer. Elle appelle ensuite les deux fonctions décrites ci-dessus pour scanner le réseau et générer le fichier INI. Enfin, elle imprime les adresses IP scannées et un message de réussite.

## Boucle principale

La boucle principale du script appelle la fonction `main()` toutes les deux minutes. Cela permet de scanner le réseau régulièrement et de mettre à jour le fichier INI avec les dernières adresses IP découvertes.

## Configuration

Pour utiliser ce script, vous devez définir le réseau à scanner et le nom du fichier INI à générer. Ces valeurs sont définies dans la fonction `main()`.

```python
network_to_scan =  "192.168.1.1-255"
inventory_ini_filename = "inventory.ini"
```

Remplacez `"192.168.1.1-255"` par le réseau que vous souhaitez scanner et `"inventory.ini"` par le nom du fichier INI que vous souhaitez générer.