# Documentation Technique pour le script `front.py`

Ce script Python utilise le framework Flask pour créer une application web qui affiche des informations sur des machines virtuelles à partir d'une base de données SQLite.

## Dépendances

- `flask` : Un framework web pour Python.
- `sqlite3` : Une bibliothèque Python pour travailler avec les bases de données SQLite.

## Routes

### `@app.route('/')`

Cette route affiche un tableau de bord avec les dernières informations de chaque machine virtuelle. Les informations sont récupérées à partir d'une base de données SQLite.

### `@app.route('/api/data/<metric>')`

Cette route renvoie un JSON avec les informations de la base de données pour une métrique spécifique (CPU, RAM, Disk). Les données sont triées par timestamp et limitées aux 200 dernières entrées.

## Fonction principale

La fonction principale du script lance l'application Flask sur le port 5013 et écoute sur toutes les interfaces réseau.

## Configuration

Pour utiliser ce script, vous devez avoir une base de données SQLite avec une table `data` contenant les informations des machines virtuelles. La base de données doit être située à `data/collecte.db`.

```python
if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5013)
```