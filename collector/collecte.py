import os
import paramiko
import sqlite3
import time
from datetime import datetime
import threading
import pytz
import csv
import psutil

def retrieve_logs(ssh_client, remote_log_path):
    _, stdout, _ = ssh_client.exec_command(f"cat {remote_log_path}")
    logs = stdout.read().decode('utf-8')
    return logs

def save_to_csv(logs, filename):
    filename = os.path.join('data/logs', filename)
    if not os.path.exists(filename):
        with open(filename, 'w', newline='') as csvfile:
            csv_writer = csv.writer(csvfile)
            csv_writer.writerow([datetime.now().strftime('%Y-%m-%d %H:%M:%S'), logs])
    else:
        with open(filename, 'a', newline='') as csvfile:
            csv_writer = csv.writer(csvfile)
            csv_writer.writerow([datetime.now().strftime('%Y-%m-%d %H:%M:%S'), logs])

def log(ssh_client):
    remote_log_path = '/var/log/syslog'
    current_time = datetime.now()
    filename = f"{current_time.strftime('%Y-%m-%d')}_logs.csv"
    logs = retrieve_logs(ssh_client, remote_log_path)
    save_to_csv(logs, filename)

## Common Collector
# Ouvrir une connexion à la base de données
with sqlite3.connect('data/collecte.db') as conn:
    c = conn.cursor()

    # Créer une table si elle n'existe pas déjà
    c.execute('''
        CREATE TABLE IF NOT EXISTS data (
            id TEXT,
            cpu_usage TEXT,
            used_memory TEXT,
            total_memory TEXT,
            free_memory TEXT,
            network_stats TEXT,
            used_disk TEXT,
            total_disk TEXT,
            free_disk TEXT,
            timestamp DATETIME
        )
    ''')

# Fonction pour collecter et stocker les informations de chaque VM
def collect():
        vms = [
            {'id': 'VM1', 'ip': '192.168.1.54', 'username': 'user', 'password': 'bonjour'},
            {'id': 'VM2', 'ip': '192.168.1.60', 'username': 'user', 'password': 'bonjour'},
        ]

        all_vm_data = []

        for vm in vms:
            ssh = paramiko.SSHClient()
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

            vm_data = {}

            try:
                ssh.connect(vm['ip'], username=vm['username'], password=vm['password'])
                #print(f"Connected to VM at {vm['ip']}")

                log(ssh)

                # Récupération de l'utilisation du CPU
                stdin, stdout, stderr = ssh.exec_command('top -b -n1 | grep "Cpu(s)" | awk \'{print $2 + $4}\'')
                cpu_usage = (stdout.read().decode('utf-8').strip())
                    
                #Récupération de l'utilisation de la mémoire
                stdin, stdout, stderr = ssh.exec_command('free -m')
                output = stdout.read().decode('utf-8')
                lines = output.split('\n')
                memory_line = lines[1].split()
                total_memory = int(memory_line[1])
                used_memory = int(memory_line[2])
                memory_usage_percentage = round((used_memory / total_memory) * 100, 2)

                # Exécuter la commande pour obtenir le nom de toutes les interfaces réseau
                stdin, stdout, stderr = ssh.exec_command('ls /sys/class/net')
                network_interfaces = stdout.read().decode().strip().split('\n')

                network_stats = []

                for interface in network_interfaces:
                    # Exécuter la commande pour obtenir l'adresse IP
                    stdin, stdout, stderr = ssh.exec_command(f"ip -f inet addr show {interface} | grep -Po '(?<=inet )[\\d.]+'")
                    ip_address = stdout.read().decode().strip()

                    # Exécuter la commande pour obtenir l'adresse MAC
                    stdin, stdout, stderr = ssh.exec_command(f'cat /sys/class/net/{interface}/address')
                    mac_address = stdout.read().decode().strip()

                    # Exécuter la commande pour obtenir l'état de la carte réseau
                    stdin, stdout, stderr = ssh.exec_command(f'cat /sys/class/net/{interface}/operstate')
                    network_state = stdout.read().decode().strip()

                    network_stats.append({
                        'interface': interface,
                        'ip_address': ip_address,
                        'mac_address': mac_address,
                        'network_state': network_state
                    })

                #Vérification de la connectivité Internet
                stdin, stdout, stderr = ssh.exec_command('ping -c 1 8.8.8.8')
                internet_connectivity = '1 packets transmitted, 1 received' in stdout.read().decode('utf-8')
                internet_connectivity_str = f"Internet connectivity: {'OK' if internet_connectivity else 'NOK'}"

                # Convert network_stats to a string
                network_stats_str = '\n'.join([f"Interface: {stat['interface']}, IP: {stat['ip_address']}, MAC: {stat['mac_address']}, State: {stat['network_state']}" for stat in network_stats])

                # Append internet connectivity to the string
                network_stats_str += '\n' + internet_connectivity_str

                
                #Récupération de l'utilisation de l'espace disque
                stdin, stdout, stderr = ssh.exec_command('df -h /')
                output = stdout.read().decode('utf-8')
                lines = output.split('\n')
                disk_line = lines[1].split()
                percent_used_disk = disk_line[4]
                total_disk = disk_line[1]
                free_disk = disk_line[3]
                    
                paris_timezone = pytz.timezone('Europe/Paris')
                heure_actuelle_paris = datetime.now(paris_timezone)
                paris_time_str = heure_actuelle_paris.strftime('%Y-%m-%d %H:%M:%S')

                vm_data['id'] = vm['id']
                vm_data['cpu_usage'] = cpu_usage
                vm_data['memory_usage_percentage'] = memory_usage_percentage
                vm_data['total_memory'] = total_memory
                vm_data['used_memory'] = used_memory
                vm_data['network_stats'] = network_stats_str
                vm_data['percent_used_disk'] = percent_used_disk
                vm_data['total_disk'] = total_disk
                vm_data['free_disk'] = free_disk
                vm_data['timestamp'] = paris_time_str

                # Insérer toutes les données dans la base de données
                with sqlite3.connect('data/collecte.db') as conn:
                    c = conn.cursor()
                    #print(vm_data)
                    c.execute('''
                        INSERT INTO data (
                            id,
                            cpu_usage,
                            used_memory,
                            total_memory,
                            free_memory,
                            network_stats,
                            used_disk,
                            total_disk,
                            free_disk,
                            timestamp
                        ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
                    ''', (
                        vm_data['id'],
                        vm_data['cpu_usage'],
                        vm_data['memory_usage_percentage'],
                        vm_data['total_memory'],
                        vm_data['used_memory'],
                        vm_data['network_stats'],
                        vm_data['percent_used_disk'],
                        vm_data['total_disk'],
                        vm_data['free_disk'],
                        vm_data['timestamp']
                    ))
                    conn.commit()
                    
                    #print(cpu_usage, memory_usage_percentage, total_memory, total_memory - used_memory, network_stats, percent_used_disk, total_disk, free_disk)

                    # Supprimer les enregistrements de plus de 24 heures
                    c.execute('''DELETE FROM data WHERE timestamp < datetime('now', '-7 day')''')
                    conn.commit()


            except TimeoutError:
                print(f"Unable to connect to VM at {vm['ip']} : the connection timed out")

            except paramiko.ssh_exception.NoValidConnectionsError:
                print(f"Unable to connect to VM at {vm['ip']} : the server is down")
            
            except paramiko.ssh_exception.AuthenticationException:
                print(f"Unable to connect to VM at {vm['ip']} : wrong credentials")

            finally:
                ssh.close()


# ## Network Collector
# def get_network_usage():
#     ssh = paramiko.SSHClient()
#     ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
#     ssh.connect("changer par l'adresse ip", username="user", password="bonjour")

#     stdin, stdout, stderr = ssh.exec_command("ifstat 1 1")

#     res = stdout.read().decode().strip().split('\n')
#     # print(res)
#     kb_in = res[2].split()[0]
#     kb_out = res[2].split()[1]
#     # print(f"in : {kb_in}, out : {kb_out}")

#     ssh.close()
#     return kb_in, kb_out

## Metric Collector
# Fonction pour collecter les métriques de performance
def collect_metrics():
    cpu_percent = psutil.cpu_percent(interval=1)
    memory_percent = psutil.virtual_memory().percent
    disk_percent = psutil.disk_usage('/').percent

    return cpu_percent, memory_percent, disk_percent

# Fonction pour enregistrer les métriques dans un fichier CSV
def save_metrics_to_csv(metrics, filename):
    timestamp = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    with open(filename, 'a', newline='') as csvfile:
        csv_writer = csv.writer(csvfile)
        csv_writer.writerow([timestamp] + list(metrics))

# Configuration des seuils d'alerte
cpu_threshold = 80  # Pourcentage d'utilisation CPU
memory_threshold = 80  # Pourcentage d'utilisation de la mémoire
disk_threshold = 80  # Pourcentage d'utilisation du disque

# Nom du fichier CSV
filename = 'data/metrics/performance_metrics.csv'

# Fonction pour collecter les métriques de performance
def metric():

    # Collecte des métriques de performance
    metrics = collect_metrics()

    # Enregistrement des métriques dans le fichier CSV
    save_metrics_to_csv(metrics, filename)

    # Vérification des seuils d'alerte
    if metrics[0] > cpu_threshold:
        print(f"Alerte : Utilisation CPU élevée ({metrics[0]}%)")

    if metrics[1] > memory_threshold:
        print(f"Alerte : Utilisation de la mémoire élevée ({metrics[1]}%)")

    if metrics[2] > disk_threshold:
        print(f"Alerte : Utilisation du disque élevée ({metrics[2]}%)")


if __name__ == '__main__':
    def collect_and_wait():
        while True:
            collect()  # Appel de la fonction de collecte des données
            metric()  # Appel de la fonction de collecte des métriques
            time.sleep(30)  # Attendre 30 secondes avant la prochaine collecte

    # Lancer la collecte de données dans un thread séparé
    t = threading.Thread(target=collect_and_wait)
    t.start()
    t.join()
    #app.run(debug=False)