from flask import Flask, jsonify, render_template
import sqlite3

app = Flask(__name__)

@app.route('/')
def tableau_de_bord():
    # Ouvrir une connexion à la base de données
    with sqlite3.connect('data/collecte.db') as conn:
        c = conn.cursor()

        # Récupérer les dernières informations de chaque VM de la base de données
        c.execute('''
            SELECT *
            FROM data
            WHERE (id, timestamp) IN (
                SELECT id, MAX(timestamp)
                FROM data
                GROUP BY id
            )
        ''')
        vm_infos = c.fetchall()

    # Passer les informations à votre template HTML
    return render_template('tableau_de_bord.j2', machines_virtuelles=vm_infos)

@app.route('/api/data/<metric>')
def api_data(metric):
    if metric == "CPU" :
        value = "cpu_usage"
    elif metric == "RAM" :
        value = "used_memory"
    elif metric == "Disk" :
        value = "used_disk"
    # Ouvrir une connexion à la base de données
    with sqlite3.connect('data/collecte.db') as conn:
        c = conn.cursor()

        # Récupérer toutes les informations de la base de données
        c.execute(f"SELECT id, {value}, timestamp FROM data ORDER BY timestamp DESC LIMIT 200")
        data = c.fetchall()

    if metric == "Disk" :
        data = [(id, float(value.replace('%', '')), timestamp) for id, value, timestamp in data]
    else :
        data = [(id, value.replace(',', '.'), timestamp) for id, value, timestamp in data]
    
    # Convertir les données en JSON et les renvoyer
    return jsonify(data)


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5013)